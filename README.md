# rFerns

[![CRAN_Status_Badge](https://www.r-pkg.org/badges/version/rFerns)](https://cran.r-project.org/package=rFerns)
[![CRAN downloads](https://cranlogs.r-pkg.org/badges/rFerns)](https://cran.rstudio.com/web/packages/rFerns/index.html)
[![codecov](https://codecov.io/gl/mbq/rFerns/branch/master/graph/badge.svg)](https://codecov.io/gl/mbq/rFerns)
[![pipeline status](https://gitlab.com/mbq/rFerns/badges/master/pipeline.svg)](https://gitlab.com/mbq/rFerns/commits/master)

rFerns is an extended [random ferns](https://cvlab.epfl.ch/research/completed/surface/ferns) implementation for [R](https://r-project.org); in comparison to original, it can handle standard information system containing both categorical and continuous attributes.
Moreover, it generates OOB error approximation and permutation-based attribute importance measure similar to [randomForest](https://www.stat.berkeley.edu/~breiman/RandomForests/cc_home.htm).
Here is a [paper with all the details](https://www.jstatsoft.org/article/view/v061i10).

rFerns is good for doing training fast, in predictable time and utilising many CPU cores; in general it is less accurate than Random Forest, yet not substantially, and obviously there are cases in which it is better.
It is also nice as a very fast variable importance source; in fact it was created to speed-up the [`Boruta` all relevant feature selector](https://notabug.org/mbq/Boruta), and [it did pretty well](https://www.biomedcentral.com/1471-2105/15/8/).
Even more, not it can produce *shadow importance*, i.e., a heuristic way to reason about significance of importance scores; the package contains a `naiveWrapper` function which implements a simple feature selector based on it, and you can read all about it [here](https://link.springer.com/chapter/10.1007/978-3-319-60438-1_30).
Finally, it is a very stochastic method, practically doing no optimisation at all; basically it is crazy that it works.
Hence, it is theoretically interesting (;

There is also a Spark version (not mine), [Sparkling Ferns](https://github.com/CeON/sparkling-ferns).


How to use
---------

Quite fresh version should be on [CRAN](https://cran.r-project.org/web/packages/rFerns/index.html), you can also install directly from GitLab with:

```r
devtools::install_gitlab('mbq/rFerns')
```

