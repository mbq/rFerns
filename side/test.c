/*   Stand-alone test of rFerns' C code

     Copyright 2011-2020 Miron B. Kursa

     This file is a redundant addition to the rFerns R package.

 rFerns is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 rFerns is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along with rFerns. If not, see http://www.gnu.org/licenses/.
*/


#include <math.h>
#include <stdio.h>
#include <float.h>
#include <stdlib.h>
#include <time.h>

#define PRINT printf

//Override tools.h macros
#define FERNS_DEFINES 7
#define ALLOCN(what,oftype,howmany) oftype* what=(oftype*)malloc(sizeof(oftype)*(howmany))
#define ALLOCNZ(what,oftype,howmany) oftype* what=(oftype*)malloc(sizeof(oftype)*(howmany)); for(uint e_=0;e_<(howmany);e_++) what[e_]=(oftype)0
#define ALLOC(what,oftype,howmany) what=(oftype*)malloc(sizeof(oftype)*(howmany))
#define ALLOCZ(what,oftype,howmany) {what=(oftype*)malloc(sizeof(oftype)*(howmany));for(uint e_=0;e_<(howmany);e_++) what[e_]=(oftype)0;}
#define IFFREE(x) if((x)) free((x))
#define FREE(x) free((x))
#define CHECK_INTERRUPT  //Nothing 
#define error(x) abort()

#include "../src/tools.h"

#define RINTEGER_MAX (~((uint32_t)0))
#define RINTEGER __rintegerf(rng)

#include "../src/fern.h"
#include "../src/forest.h"

void irisTest(R_){
 #include "iris.h"
 uint N=150;
 uint NN=8;

 uint use_threads=1;

 params Q;
 Q.numClasses=3;
 Q.D=5;
 Q.twoToD=1<<(Q.D);
 Q.numFerns=50000;
 Q.holdForest=0;
 Q.calcImp=2;
 Q.multilabel=0;
 Q.consSeed=21;
 uint nt=Q.threads=use_threads;

 uint64_t cs;
 ((uint32_t*)&cs)[0]=RINTEGER;
 ((uint32_t*)&cs)[1]=RINTEGER;
 Q.consSeed=cs;
 //Make dataset
 struct attribute *X=malloc(sizeof(struct attribute)*NN);
 uint *y=malloc(sizeof(uint)*N);
 for(uint e=0;e<NN;e++){
  X[e].numCat=0;
  X[e].x=malloc(sizeof(double)*N);
  for(uint ee=0;ee<N;ee++){
    ((double*)(X[e].x))[ee]=IrisByColumn[N*e+ee];
  }
 }
 for(uint e=0;e<50;e++) y[e]=0;
 for(uint e=50;e<100;e++) y[e]=1;
 for(uint e=100;e<150;e++) y[e]=2;

 ferns ferns;
 ferns.splitAtts=(int*)malloc(Q.D*nt*sizeof(int));
 ferns.thresholds=(thresh*)malloc(Q.D*nt*sizeof(thresh));
 ferns.scores=(double*)malloc((Q.numClasses)*(Q.twoToD)*nt*sizeof(score_t));

 //=allocateFernForest(&Q);
 uint *yp=malloc(sizeof(uint)*N);


 //** Shadow importance **//
 if(0){

  //Fire classifier
  printf("Making fern forest...\n");
  model *M=makeModel(X,NN,y,N,&ferns,&Q,_R);
  printf("Fern forest made.\n");

  if(M->imp && Q.calcImp==2){
   printf("Imp:\t(sLoss)\t(sha)\t(tries)\n");
   for(uint e=0;e<NN;e++){
    printf("Att%d\t%0.4f\t%0.4f\t%0.0f\n",e,
     M->imp[e],M->shimp[e],M->try[e]);
    if(e==3) printf("=====================================\n");
   }
  }else{
   if(M->imp && Q.calcImp==1){
    printf("Imp:\t(sLoss)\t(tries)\n");
    for(uint e=0;e<NN;e++)
     printf("Att%d\t%0.4f\t%0.0f\n",e,M->imp[e],M->try[e]);
   }
  }
  killModel(M);
 }

 //** Normal importance **//

 {
  Q.calcImp=1;
  //Fire classifier
  printf("Making fern forest...\n");
  model *M=makeModel(X,NN,y,N,&ferns,&Q,_R);
  printf("Fern forest made.\n");

  if(M->imp && Q.calcImp==2){
   printf("Imp:\t(sLoss)\t(sha)\t(tries)\n");
   for(uint e=0;e<NN;e++){
    printf("Att%d\t%0.4f\t%0.4f\t%0.0f\n",e,
     M->imp[e],M->shimp[e],M->try[e]);
    if(e==3) printf("=====================================\n");
   }
  }else{
   if(M->imp && Q.calcImp==1){
    printf("Imp:\t(sLoss)\t(tries)\n");
    for(uint e=0;e<NN;e++){
     printf("Att%d\t%0.4f\t%0.0f\n",e,M->imp[e],M->try[e]);
     if(e==3) printf("=====================================\n");
    }
   }
  }
  killModel(M);
 }

 free(ferns.splitAtts);
 free(ferns.thresholds);
 free(ferns.scores);
 free(y);
 free(yp);
 for(uint e=0;e<NN;e++){
  free(X[e].x);
 }
 free(X);
}

int main(int argc,char **argv){
 rng_t rngdata;
 rng_t *rng=&rngdata;
 SETRNG(rng,10,10);
 
 irisTest(_R);
 return(0);
}
